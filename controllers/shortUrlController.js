const validUrl = require('valid-url');
const shortid = require('shortid');
const { HOST, PORT, HOST_PROTOCOL } = process.env
const Url = require('../models/Url');
const { log } = require('../helpers/loggerHelper')
const VisitorInformation = require('../models/VisitorInformation');
const requestIp = require('request-ip');
const axios = require('axios');


module.exports.createShortUrl = async (req, res) => {

    const { userId, longUrl, urlEndDate } = req.body;
    const baseUrl = `${HOST_PROTOCOL}://${HOST}:${PORT}`;
    // return console.log(baseUrl);

    if (!userId) {
        return res.status(400).json({
            statusCode: 400,
            message: 'userId is required',
        });
    }

    if (!longUrl) {
        return res.status(400).json({
            statusCode: 400,
            message: 'longUrl is required',
        });
    }

    // Check base url
    if (!validUrl.isUri(baseUrl)) {
        return res.status(400).json('Invalid base url');
    }

    // Create url code
    const urlCode = shortid.generate();

    // Check long url
    if (validUrl.isUri(longUrl)) {
        try {
            console.log("Searching DB For", longUrl);
            let url = await Url.findOne({ longUrl });

            if (url) {
                res.status(200).json({
                    statusCode: 200,
                    message: 'URL already exist, shortUrl/shortCode not created',
                    data: {
                        shortUrl: url.shortUrl,
                        longUrl: url.longUrl
                    }
                });
            } else {
                const shortUrl = baseUrl + '/' + urlCode;
                console.log("Creating Short Code");

                let urlStoreData;
                if (!urlEndDate) {
                    let urlEndDateOneYr = new Date()
                    urlEndDateOneYr.setFullYear(urlEndDateOneYr.getFullYear() + 1);
                    urlStoreData = {
                        longUrl,
                        shortUrl,
                        urlCode,
                        cdt: new Date(),
                        urlEndDate: urlEndDateOneYr,
                        userId
                    }
                } else {
                    urlStoreData = {
                        longUrl,
                        shortUrl,
                        urlCode,
                        cdt: new Date(),
                        urlEndDate,
                        userId
                    }
                }
                url = new Url(urlStoreData);
                await url.save();
                console.log("New Short Code Generated");
                res.status(200).json({
                    statusCode: 200,
                    message: 'New Short Code Generated',
                    data: url
                });
                // res.json(url);
            }
        } catch (err) {
            log.error(err.message);
            console.error(err);
            res.status(500).json('Server error');
        }
    } else {
        res.status(400).json({
            statusCode: 400,
            message: 'Invalid long url'
        });
    }
}


module.exports.redirectLongUrl = async (req, res) => {

    try {
        const url = await Url.findOne({ urlCode: req.params.code });
        const useragent = req.useragent;
        console.log("useragent.isDesktop: ", useragent.isDesktop);

        if (url) {
            const clientIp = requestIp.getClientIp(req);
            //http://ip-api.com/json/
            //http://freegeoip.net/json/
            //https://api.ip2location.com/v2/?ip=clientIp&key={YOUR_API_KEY}

            //const staticIp = "2402:3a80:15f2:3397:741e:aedd:edeb:5627";

            let getUserDetails = `https://www.iplocate.io/api/lookup/${clientIp}`;
            const responseData = await axios.get(getUserDetails);

            let country, countryCode, city;
            if (responseData) {
                country = responseData.data.country;
                countryCode = responseData.data.country_code;
                city = responseData.data.city;
            } else {
                country = 'Other';
                countryCode = 'Other';
                city = 'Other';
            }

            // url.clicks++; // not required to store the clicks
            // await url.save();

            const visitorInformation = new VisitorInformation({
                urlId: url._id,
                shortUrl: url.shortUrl,
                ...useragent,
                ipAddress: clientIp,
                country,
                countryCode,
                city,
                cdt: Date.now(),
            });
            await visitorInformation.save();

            // redirect to the longUrl of the respective shortUrl's code
            return res.redirect(url.longUrl);

        } else {
            return res.status(404).json('No url found');
        }

    } catch (err) {
        log.error(err.message)
        console.error(err);
        res.status(500).json('Server error');
    }
}


module.exports.getShortUrlList = async (req, res) => {

    try {
        let { userId, pageNo, limit } = req.body;
        console.log('{ userId, pageNo, limit }', { userId, pageNo, limit });
        if (!userId || !pageNo || !limit) {
            return res.status(400).json({
                statusCode: 400,
                message: "All fields are required: userId, pageNo & limit"
            });
        }
        pageNo = parseInt(pageNo)
        limit = parseInt(limit)

        let urlListQuery = { userId, urlEndDate: { $gte: (new Date) } }
        let [urlList, count] = await Promise.all([
            Url.aggregate([
                { $match: urlListQuery },
                { $project: { __v: 0 } },
                { $sort: { cdt: -1 } },
                { $skip: (pageNo - 1) * limit },
                { $limit: limit }
            ]),
            Url.aggregate([
                { $match: urlListQuery },
                { $count: "urlListCount" }
            ])
        ])

        res.status(200).json({
            statusCode: 200,
            message: "Retrieved url details successfully.",
            data: urlList,
            count
        });
    } catch (error) {
        log.error(error.message)
        console.log(error);
        return res.status(500).json({
            statusCode: 500,
            message: "Server error"
        });
    }
}

