const express = require("express");
var cors = require('cors')
const useragent = require("express-useragent");
require("dotenv").config();
const { HOST, PORT, HOST_PROTOCOL } = process.env;


const connectDB = require("./config/db");

let app = express();
app.use(cors());
app.use(useragent.express());

app.use(express.json());
app.use(express.urlencoded({ extended: false }));

// Define Routes
app.use("/", require("./routes/index"));
app.use("/api/url", require("./routes/url"));

connectDB()
	.then(() => {
		app.listen(PORT, () => console.log(`Server running on ${HOST_PROTOCOL}://${HOST}:${PORT}`));
	})
	.catch((err) => console.log(err));
