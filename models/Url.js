const mongoose = require('mongoose');

const urlSchema = new mongoose.Schema({
  urlCode: String,
  userId: String,
  longUrl: String,
  shortUrl: String,
  cdt: { type: Date },
  urlEndDate: { type: Date },
});

module.exports = mongoose.model('Url', urlSchema);
