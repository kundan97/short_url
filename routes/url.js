const express = require('express');
const router = express.Router();
const shortUrlController = require('../controllers/shortUrlController')


// @route     POST /api/url/shorten
// @desc      Create short URL
router.post('/shorten', shortUrlController.createShortUrl);

module.exports = router;
