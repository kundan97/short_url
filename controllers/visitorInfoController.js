const { log } = require('../helpers/loggerHelper')
const VisitorInformation = require('../models/VisitorInformation');
const moment = require('moment');


module.exports.getVisitorInfoWeekWise = async (req, res) => {
    try {
        let { urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate } = req.body;

        console.log('{ urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate }', { urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate });
        //----------------------
        // req validation
        if (!urlId) {
            return res.status(400).json({
                statusCode: 400,
                message: "urlId is required"
            });
        }
        //----------------------
        if ((!startDate && endDate) || (startDate && !endDate)) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be blank"
            });
        }
        if ((new Date(startDate)) > (new Date(endDate))) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate should not be greater than endDate"
            });
        }
        if (!startDate && !endDate) {
            startDate = moment().subtract(6, 'd').format('YYYY-MM-DD');
            endDate = moment().format('YYYY-MM-DD')
        }

        let date_diff = ((new Date(endDate)) - (new Date(startDate)) + 86400000) // (diff + 1 day time),
        // diff is 6 days, res in miliseconds. This is done to include startDate & endDate in result count

        let OneWeekTimeStamp = 1000 * 60 * 60 * 24 * 7

        if (date_diff > OneWeekTimeStamp) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be more than one week difference"
            });
        }
        if (date_diff < OneWeekTimeStamp) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be less than one week difference"
            });
        }
        //----------------------

        let urlQuery = { urlId } // default search on the basis of userId & urlId

        //---------------------
        // filtration:
        if (country) {
            urlQuery = { ...urlQuery, country }
        }
        if (browser) {
            urlQuery = { ...urlQuery, browser }
        }
        if (os) {
            urlQuery = { ...urlQuery, os }
        }
        if (isMobile) {
            urlQuery = { ...urlQuery, isMobile: "true" }
        }
        if (isTablet) {
            urlQuery = { ...urlQuery, isTablet: "true" }
        }
        if (isiPad) {
            urlQuery = { ...urlQuery, isiPad: "true" }
        }
        if (isDesktop) {
            urlQuery = { ...urlQuery, isDesktop: "true" }
        }
        //---------------------

        // Day 1 query
        startDateDay1 = moment(endDate).subtract(6, 'd').format('YYYY-MM-DD');
        endDateDay1 = moment(endDate).subtract(6, 'd').format('YYYY-MM-DD')
        urlQueryDay1 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateDay1}T00:00:00.000Z`),
                "$lte": new Date(`${endDateDay1}T23:59:59.999Z`)
            }
        }

        // Day 2 query
        startDateDay2 = moment(endDate).subtract(5, 'd').format('YYYY-MM-DD');
        endDateDay2 = moment(endDate).subtract(5, 'd').format('YYYY-MM-DD')
        urlQueryDay2 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateDay2}T00:00:00.000Z`),
                "$lte": new Date(`${endDateDay2}T23:59:59.999Z`)
            }
        }

        // Day 3 query
        startDateDay3 = moment(endDate).subtract(4, 'd').format('YYYY-MM-DD');
        endDateDay3 = moment(endDate).subtract(4, 'd').format('YYYY-MM-DD')
        urlQueryDay3 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateDay3}T00:00:00.000Z`),
                "$lte": new Date(`${endDateDay3}T23:59:59.999Z`)
            }
        }

        // Day 4 query
        startDateDay4 = moment(endDate).subtract(3, 'd').format('YYYY-MM-DD');
        endDateDay4 = moment(endDate).subtract(3, 'd').format('YYYY-MM-DD')
        urlQueryDay4 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateDay4}T00:00:00.000Z`),
                "$lte": new Date(`${endDateDay4}T23:59:59.999Z`)
            }
        }

        // Day 5 query
        startDateDay5 = moment(endDate).subtract(2, 'd').format('YYYY-MM-DD');
        endDateDay5 = moment(endDate).subtract(2, 'd').format('YYYY-MM-DD')
        urlQueryDay5 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateDay5}T00:00:00.000Z`),
                "$lte": new Date(`${endDateDay5}T23:59:59.999Z`)
            }
        }

        // Day 6 query
        startDateDay6 = moment(endDate).subtract(1, 'd').format('YYYY-MM-DD');
        endDateDay6 = moment(endDate).subtract(1, 'd').format('YYYY-MM-DD')
        urlQueryDay6 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateDay6}T00:00:00.000Z`),
                "$lte": new Date(`${endDateDay6}T23:59:59.999Z`)
            }
        }

        // Day 7 query
        startDateDay7 = moment(endDate).format('YYYY-MM-DD');
        endDateDay7 = moment(endDate).format('YYYY-MM-DD')
        urlQueryDay7 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateDay7}T00:00:00.000Z`),
                "$lte": new Date(`${endDateDay7}T23:59:59.999Z`)
            }
        }

        let [totalCountDay1, uniqueCountDay1, totalCountDay2, uniqueCountDay2, totalCountDay3, uniqueCountDay3, totalCountDay4, uniqueCountDay4, totalCountDay5, uniqueCountDay5, totalCountDay6, uniqueCountDay6, totalCountDay7, uniqueCountDay7] = await Promise.all([

            // Day 1 counts
            VisitorInformation.countDocuments(urlQueryDay1),
            VisitorInformation.aggregate([
                { $match: urlQueryDay1 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountDay1" }
            ]),
            // Day 2 counts
            VisitorInformation.countDocuments(urlQueryDay2),
            VisitorInformation.aggregate([
                { $match: urlQueryDay2 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountDay2" }
            ]),
            // Day 3 counts
            VisitorInformation.countDocuments(urlQueryDay3),
            VisitorInformation.aggregate([
                { $match: urlQueryDay3 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountDay3" }
            ]),
            // Day 4 counts
            VisitorInformation.countDocuments(urlQueryDay4),
            VisitorInformation.aggregate([
                { $match: urlQueryDay4 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountDay4" }
            ]),
            // Day 5 counts
            VisitorInformation.countDocuments(urlQueryDay5),
            VisitorInformation.aggregate([
                { $match: urlQueryDay5 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountDay5" }
            ]),
            // Day 6 counts
            VisitorInformation.countDocuments(urlQueryDay6),
            VisitorInformation.aggregate([
                { $match: urlQueryDay6 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountDay6" }
            ]),
            // Day 7 counts
            VisitorInformation.countDocuments(urlQueryDay7),
            VisitorInformation.aggregate([
                { $match: urlQueryDay7 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountDay7" }
            ]),
        ])

        if (uniqueCountDay1.length == 0) {
            uniqueCountDay1 = 0
        } else {
            uniqueCountDay1 = uniqueCountDay1[0].uniqueCountDay1
        }
        if (uniqueCountDay2.length == 0) {
            uniqueCountDay2 = 0
        } else {
            uniqueCountDay2 = uniqueCountDay2[0].uniqueCountDay2
        }
        if (uniqueCountDay3.length == 0) {
            uniqueCountDay3 = 0
        } else {
            uniqueCountDay3 = uniqueCountDay3[0].uniqueCountDay3
        }
        if (uniqueCountDay4.length == 0) {
            uniqueCountDay4 = 0
        } else {
            uniqueCountDay4 = uniqueCountDay4[0].uniqueCountDay4
        }
        if (uniqueCountDay5.length == 0) {
            uniqueCountDay5 = 0
        } else {
            uniqueCountDay5 = uniqueCountDay5[0].uniqueCountDay5
        }
        if (uniqueCountDay6.length == 0) {
            uniqueCountDay6 = 0
        } else {
            uniqueCountDay6 = uniqueCountDay6[0].uniqueCountDay6
        }
        if (uniqueCountDay7.length == 0) {
            uniqueCountDay7 = 0
        } else {
            uniqueCountDay7 = uniqueCountDay7[0].uniqueCountDay7
        }

        resData = {
            day1: { startDate: startDateDay1, endDate: endDateDay1, totalCount: totalCountDay1, uniqueCount: uniqueCountDay1 },
            day2: { startDate: startDateDay2, endDate: endDateDay2, totalCount: totalCountDay2, uniqueCount: uniqueCountDay2 },
            day3: { startDate: startDateDay3, endDate: endDateDay3, totalCount: totalCountDay3, uniqueCount: uniqueCountDay3 },
            day4: { startDate: startDateDay4, endDate: endDateDay4, totalCount: totalCountDay4, uniqueCount: uniqueCountDay4 },
            day5: { startDate: startDateDay5, endDate: endDateDay5, totalCount: totalCountDay5, uniqueCount: uniqueCountDay5 },
            day6: { startDate: startDateDay6, endDate: endDateDay6, totalCount: totalCountDay6, uniqueCount: uniqueCountDay6 },
            day7: { startDate: startDateDay7, endDate: endDateDay7, totalCount: totalCountDay7, uniqueCount: uniqueCountDay7 },
        }

        res.status(200).json({
            statusCode: 200,
            message: "Retrieved short url visitor count successfully",
            data: resData
        });
    } catch (error) {
        log.error(error.message)
        console.log(error);
        return res.status(500).json({
            statusCode: 500,
            message: "Server error"
        });
    }

}


module.exports.getVisitorInfoMonthWise = async (req, res) => {
    try {
        let { urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate } = req.body;
        console.log('{ urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate }', { urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate });
        //----------------------
        // req validation
        if (!urlId) {
            return res.status(400).json({
                statusCode: 400,
                message: "urlId is required"
            });
        }
        //----------------------
        if ((!startDate && endDate) || (startDate && !endDate)) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be blank"
            });
        }
        if ((new Date(startDate)) > (new Date(endDate))) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate should not be greater than endDate"
            });
        }
        if (!startDate && !endDate) {
            startDate = moment().subtract(29, 'd').format('YYYY-MM-DD');
            endDate = moment().format('YYYY-MM-DD')
        }

        let date_diff = ((new Date(endDate)) - (new Date(startDate)) + 86400000) // (diff + 1 day time),
        // diff is 29 days, res in miliseconds. This is done to include startDate & endDate in result count

        let OneMonthTimeStamp = 1000 * 60 * 60 * 24 * 30
        let OneYearTimeStamp = 1000 * 60 * 60 * 24 * 365


        if (date_diff > OneMonthTimeStamp) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be more than 30 days difference"
            });
        }
        if (date_diff < OneMonthTimeStamp) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be less than 30 days difference"
            });
        }
        let startFromNow = new Date() - new Date(startDate)
        if (startFromNow > OneYearTimeStamp) {
            return res.status(400).json({
                statusCode: 400,
                message: "Data not available for more than one year"
            });
        }
        //----------------------

        let urlQuery = { urlId } // default search on the basis of userId & urlId

        //---------------------
        // filtration:
        if (country) {
            urlQuery = { ...urlQuery, country }
        }
        if (browser) {
            urlQuery = { ...urlQuery, browser }
        }
        if (os) {
            urlQuery = { ...urlQuery, os }
        }
        if (isMobile) {
            urlQuery = { ...urlQuery, isMobile: "true" }
        }
        if (isTablet) {
            urlQuery = { ...urlQuery, isTablet: "true" }
        }
        if (isiPad) {
            urlQuery = { ...urlQuery, isiPad: "true" }
        }
        if (isDesktop) {
            urlQuery = { ...urlQuery, isDesktop: "true" }
        }
        //---------------------

        let dateStartTime = 'T00:00:00.000Z'
        let dateEndTime = 'T23:59:59.999Z'

        // Week 1 query
        startDateWeek1 = moment(endDate).subtract(29, 'd').format('YYYY-MM-DD');
        endDateWeek1 = moment(startDateWeek1).add(6, 'd').format('YYYY-MM-DD')
        urlQueryWeek1 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateWeek1}${dateStartTime}`),
                "$lte": new Date(`${endDateWeek1}${dateEndTime}`)
            }
        }

        // Week 2 query
        startDateWeek2 = moment(endDateWeek1).add(1, 'd').format('YYYY-MM-DD');
        endDateWeek2 = moment(startDateWeek2).add(6, 'd').format('YYYY-MM-DD')
        urlQueryWeek2 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateWeek2}${dateStartTime}`),
                "$lte": new Date(`${endDateWeek2}${dateEndTime}`)
            }
        }

        // Week 3 query
        startDateWeek3 = moment(endDateWeek2).add(1, 'd').format('YYYY-MM-DD');
        endDateWeek3 = moment(startDateWeek3).add(6, 'd').format('YYYY-MM-DD')
        urlQueryWeek3 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateWeek3}${dateStartTime}`),
                "$lte": new Date(`${endDateWeek3}${dateEndTime}`)
            }
        }

        // Week 4 query
        startDateWeek4 = moment(endDateWeek3).add(1, 'd').format('YYYY-MM-DD');
        endDateWeek4 = moment(startDateWeek4).add(6, 'd').format('YYYY-MM-DD')
        urlQueryWeek4 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateWeek4}${dateStartTime}`),
                "$lte": new Date(`${endDateWeek4}${dateEndTime}`)
            }
        }

        restDay = (new Date(endDate) - new Date(endDateWeek4)) / 1000 / 60 / 60 / 24

        // Week 5 query with restDay
        startDateWeek5 = moment(endDateWeek4).add(1, 'd').format('YYYY-MM-DD');
        endDateWeek5 = moment(endDateWeek4).add((restDay), 'd').format('YYYY-MM-DD')
        urlQueryWeek5 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateWeek5}${dateStartTime}`),
                "$lte": new Date(`${endDateWeek5}${dateEndTime}`)
            }
        }

        let [totalCountWeek1, uniqueCountWeek1, totalCountWeek2, uniqueCountWeek2, totalCountWeek3, uniqueCountWeek3, totalCountWeek4, uniqueCountWeek4, totalCountWeek5, uniqueCountWeek5] = await Promise.all([
            // Week 1 counts
            VisitorInformation.countDocuments(urlQueryWeek1),
            VisitorInformation.aggregate([
                { $match: urlQueryWeek1 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountWeek1" }
            ]),
            // Week 2 counts
            VisitorInformation.countDocuments(urlQueryWeek2),
            VisitorInformation.aggregate([
                { $match: urlQueryWeek2 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountWeek2" }
            ]),
            // Week 3 counts
            VisitorInformation.countDocuments(urlQueryWeek3),
            VisitorInformation.aggregate([
                { $match: urlQueryWeek3 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountWeek3" }
            ]),
            // Week 4 counts
            VisitorInformation.countDocuments(urlQueryWeek4),
            VisitorInformation.aggregate([
                { $match: urlQueryWeek4 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountWeek4" }
            ]),
            // Week 5 counts
            VisitorInformation.countDocuments(urlQueryWeek5),
            VisitorInformation.aggregate([
                { $match: urlQueryWeek5 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountWeek5" }
            ]),
        ])

        if (uniqueCountWeek1.length == 0) {
            uniqueCountWeek1 = 0
        } else {
            uniqueCountWeek1 = uniqueCountWeek1[0].uniqueCountWeek1
        }
        if (uniqueCountWeek2.length == 0) {
            uniqueCountWeek2 = 0
        } else {
            uniqueCountWeek2 = uniqueCountWeek2[0].uniqueCountWeek2
        }
        if (uniqueCountWeek3.length == 0) {
            uniqueCountWeek3 = 0
        } else {
            uniqueCountWeek3 = uniqueCountWeek3[0].uniqueCountWeek3
        }
        if (uniqueCountWeek4.length == 0) {
            uniqueCountWeek4 = 0
        } else {
            uniqueCountWeek4 = uniqueCountWeek4[0].uniqueCountWeek4
        }
        if (uniqueCountWeek5.length == 0) {
            uniqueCountWeek5 = 0
        } else {
            uniqueCountWeek5 = uniqueCountWeek5[0].uniqueCountWeek5
        }


        resData = {
            Week1: { startDate: startDateWeek1, endDate: endDateWeek1, totalCount: totalCountWeek1, uniqueCount: uniqueCountWeek1 },
            Week2: { startDate: startDateWeek2, endDate: endDateWeek2, totalCount: totalCountWeek2, uniqueCount: uniqueCountWeek2 },
            Week3: { startDate: startDateWeek3, endDate: endDateWeek3, totalCount: totalCountWeek3, uniqueCount: uniqueCountWeek3 },
            Week4: { startDate: startDateWeek4, endDate: endDateWeek4, totalCount: totalCountWeek4, uniqueCount: uniqueCountWeek4 },
            Week5: { startDate: startDateWeek5, endDate: endDateWeek5, totalCount: totalCountWeek5, uniqueCount: uniqueCountWeek5 },
        }

        res.status(200).json({
            statusCode: 200,
            message: "Retrieved short url visitor count successfully",
            data: resData
        });
    } catch (error) {
        log.error(error.message)
        console.log(error);
        return res.status(500).json({
            statusCode: 500,
            message: "Server error"
        });
    }

}


module.exports.getVisitorInfoLastOneYear = async (req, res) => {
    try {
        let { urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate } = req.body;
        console.log('{ urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate }', { urlId, country, browser, os, isMobile, isTablet, isiPad, isDesktop, startDate, endDate });
        //----------------------
        // req validation
        if (!urlId) {
            return res.status(400).json({
                statusCode: 400,
                message: "urlId is required"
            });
        }
        //----------------------
        if ((!startDate && endDate) || (startDate && !endDate)) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be blank"
            });
        }
        if ((new Date(startDate)) > (new Date(endDate))) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate should not be greater than endDate"
            });
        }
        if (!startDate && !endDate) {
            startDate = moment(this.endDate).subtract(364, 'd').format('YYYY-MM-DD')
            endDate = moment().format('YYYY-MM-DD')
        }

        let date_diff = ((new Date(endDate)) - (new Date(startDate)) + 86400000) // (diff + 1 day time),
        // diff is 29 days, res in miliseconds. This is done to include startDate & endDate in result count

        let OneYearTimeStamp = 1000 * 60 * 60 * 24 * 365

        if (date_diff > OneYearTimeStamp) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be more than 365 days difference"
            });
        }
        if (date_diff < OneYearTimeStamp) {
            return res.status(400).json({
                statusCode: 400,
                message: "startDate & endDate should not be less than 365 days difference"
            });
        }
        //----------------------

        let urlQuery = { urlId } // default search on the basis of userId & urlId

        //---------------------
        // filtration:
        if (country) {
            urlQuery = { ...urlQuery, country }
        }
        if (browser) {
            urlQuery = { ...urlQuery, browser }
        }
        if (os) {
            urlQuery = { ...urlQuery, os }
        }
        if (isMobile) {
            urlQuery = { ...urlQuery, isMobile: "true" }
        }
        if (isTablet) {
            urlQuery = { ...urlQuery, isTablet: "true" }
        }
        if (isiPad) {
            urlQuery = { ...urlQuery, isiPad: "true" }
        }
        if (isDesktop) {
            urlQuery = { ...urlQuery, isDesktop: "true" }
        }
        //---------------------

        let dateStartTime = 'T00:00:00.000Z'
        let dateEndTime = 'T23:59:59.999Z'

        // Month 1 query
        startDateMonth1 = moment(endDate).subtract(11, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth1 = moment(endDate).subtract(11, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth1 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth1}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth1}${dateEndTime}`)
            }
        }

        // Month 2 query
        startDateMonth2 = moment(endDate).subtract(10, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth2 = moment(endDate).subtract(10, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth2 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth2}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth2}${dateEndTime}`)
            }
        }

        // Month 3 query
        startDateMonth3 = moment(endDate).subtract(9, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth3 = moment(endDate).subtract(9, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth3 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth3}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth3}${dateEndTime}`)
            }
        }

        // Month 4 query
        startDateMonth4 = moment(endDate).subtract(8, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth4 = moment(endDate).subtract(8, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth4 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth4}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth4}${dateEndTime}`)
            }
        }

        // Month 5 query
        startDateMonth5 = moment(endDate).subtract(7, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth5 = moment(endDate).subtract(7, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth5 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth5}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth5}${dateEndTime}`)
            }
        }

        // Month 6 query
        startDateMonth6 = moment(endDate).subtract(6, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth6 = moment(endDate).subtract(6, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth6 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth6}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth6}${dateEndTime}`)
            }
        }

        // Month 7 query
        startDateMonth7 = moment(endDate).subtract(5, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth7 = moment(endDate).subtract(5, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth7 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth7}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth7}${dateEndTime}`)
            }
        }

        // Month 8 query
        startDateMonth8 = moment(endDate).subtract(4, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth8 = moment(endDate).subtract(4, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth8 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth8}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth8}${dateEndTime}`)
            }
        }

        // Month 9 query
        startDateMonth9 = moment(endDate).subtract(3, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth9 = moment(endDate).subtract(3, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth9 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth9}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth9}${dateEndTime}`)
            }
        }

        // Month 10 query
        startDateMonth10 = moment(endDate).subtract(2, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth10 = moment(endDate).subtract(2, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth10 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth10}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth10}${dateEndTime}`)
            }
        }

        // Month 11 query
        startDateMonth11 = moment(endDate).subtract(1, 'month').startOf('month').format('YYYY-MM-DD')
        endDateMonth11 = moment(endDate).subtract(1, 'month').endOf('month').format('YYYY-MM-DD')
        urlQueryMonth11 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth11}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth11}${dateEndTime}`)
            }
        }

        // Month 12 query
        startDateMonth12 = moment(endDate).startOf('month').format('YYYY-MM-DD')
        endDateMonth12 = moment(endDate).endOf('month').format('YYYY-MM-DD')
        urlQueryMonth12 = {
            ...urlQuery,
            cdt: {
                "$gte": new Date(`${startDateMonth12}${dateStartTime}`),
                "$lte": new Date(`${endDateMonth12}${dateEndTime}`)
            }
        }

        let [totalCountMonth1, uniqueCountMonth1, totalCountMonth2, uniqueCountMonth2, totalCountMonth3, uniqueCountMonth3, totalCountMonth4, uniqueCountMonth4, totalCountMonth5, uniqueCountMonth5, totalCountMonth6, uniqueCountMonth6, totalCountMonth7, uniqueCountMonth7, totalCountMonth8, uniqueCountMonth8, totalCountMonth9, uniqueCountMonth9, totalCountMonth10, uniqueCountMonth10, totalCountMonth11, uniqueCountMonth11, totalCountMonth12, uniqueCountMonth12] = await Promise.all([
            // Month 1 counts
            VisitorInformation.countDocuments(urlQueryMonth1),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth1 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth1" }
            ]),
            // Month 2 counts
            VisitorInformation.countDocuments(urlQueryMonth2),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth2 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth2" }
            ]),
            // Month 3 counts
            VisitorInformation.countDocuments(urlQueryMonth3),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth3 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth3" }
            ]),
            // Month 4 counts
            VisitorInformation.countDocuments(urlQueryMonth4),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth4 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth4" }
            ]),
            // Month 5 counts
            VisitorInformation.countDocuments(urlQueryMonth5),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth5 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth5" }
            ]),
            // Month 5 counts
            VisitorInformation.countDocuments(urlQueryMonth6),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth6 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth6" }
            ]),
            // Month 5 counts
            VisitorInformation.countDocuments(urlQueryMonth7),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth7 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth7" }
            ]),
            // Month 5 counts
            VisitorInformation.countDocuments(urlQueryMonth8),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth8 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth8" }
            ]),
            // Month 5 counts
            VisitorInformation.countDocuments(urlQueryMonth9),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth9 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth9" }
            ]),
            // Month 5 counts
            VisitorInformation.countDocuments(urlQueryMonth10),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth10 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth10" }
            ]),
            // Month 5 counts
            VisitorInformation.countDocuments(urlQueryMonth11),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth11 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth11" }
            ]),
            // Month 5 counts
            VisitorInformation.countDocuments(urlQueryMonth12),
            VisitorInformation.aggregate([
                { $match: urlQueryMonth12 },
                {
                    $group: {
                        _id: "$ipAddress",
                        "country": { $first: "$country" },
                        "city": { $first: "$city" },
                        "browser": { $first: "$browser" },
                        "os": { $first: "$os" },
                        view_count: { $sum: 1 }
                    }
                },
                { $count: "uniqueCountMonth12" }
            ]),
        ])

        if (uniqueCountMonth1.length == 0) {
            uniqueCountMonth1 = 0
        } else {
            uniqueCountMonth1 = uniqueCountMonth1[0].uniqueCountMonth1
        }
        if (uniqueCountMonth2.length == 0) {
            uniqueCountMonth2 = 0
        } else {
            uniqueCountMonth2 = uniqueCountMonth2[0].uniqueCountMonth2
        }
        if (uniqueCountMonth3.length == 0) {
            uniqueCountMonth3 = 0
        } else {
            uniqueCountMonth3 = uniqueCountMonth3[0].uniqueCountMonth3
        }
        if (uniqueCountMonth4.length == 0) {
            uniqueCountMonth4 = 0
        } else {
            uniqueCountMonth4 = uniqueCountMonth4[0].uniqueCountMonth4
        }
        if (uniqueCountMonth5.length == 0) {
            uniqueCountMonth5 = 0
        } else {
            uniqueCountMonth5 = uniqueCountMonth5[0].uniqueCountMonth5
        }
        if (uniqueCountMonth6.length == 0) {
            uniqueCountMonth6 = 0
        } else {
            uniqueCountMonth6 = uniqueCountMonth6[0].uniqueCountMonth6
        }
        if (uniqueCountMonth7.length == 0) {
            uniqueCountMonth7 = 0
        } else {
            uniqueCountMonth7 = uniqueCountMonth7[0].uniqueCountMonth7
        }
        if (uniqueCountMonth8.length == 0) {
            uniqueCountMonth8 = 0
        } else {
            uniqueCountMonth8 = uniqueCountMonth8[0].uniqueCountMonth8
        }
        if (uniqueCountMonth9.length == 0) {
            uniqueCountMonth9 = 0
        } else {
            uniqueCountMonth9 = uniqueCountMonth9[0].uniqueCountMonth9
        }
        if (uniqueCountMonth10.length == 0) {
            uniqueCountMonth10 = 0
        } else {
            uniqueCountMonth10 = uniqueCountMonth10[0].uniqueCountMonth10
        }
        if (uniqueCountMonth11.length == 0) {
            uniqueCountMonth11 = 0
        } else {
            uniqueCountMonth11 = uniqueCountMonth11[0].uniqueCountMonth11
        }
        if (uniqueCountMonth12.length == 0) {
            uniqueCountMonth12 = 0
        } else {
            uniqueCountMonth12 = uniqueCountMonth12[0].uniqueCountMonth12
        }


        resData = {
            Month1: { startDate: startDateMonth1, endDate: endDateMonth1, totalCount: totalCountMonth1, uniqueCount: uniqueCountMonth1 },
            Month2: { startDate: startDateMonth2, endDate: endDateMonth2, totalCount: totalCountMonth2, uniqueCount: uniqueCountMonth2 },
            Month3: { startDate: startDateMonth3, endDate: endDateMonth3, totalCount: totalCountMonth3, uniqueCount: uniqueCountMonth3 },
            Month4: { startDate: startDateMonth4, endDate: endDateMonth4, totalCount: totalCountMonth4, uniqueCount: uniqueCountMonth4 },
            Month5: { startDate: startDateMonth5, endDate: endDateMonth5, totalCount: totalCountMonth5, uniqueCount: uniqueCountMonth5 },
            Month6: { startDate: startDateMonth6, endDate: endDateMonth6, totalCount: totalCountMonth6, uniqueCount: uniqueCountMonth6 },
            Month7: { startDate: startDateMonth7, endDate: endDateMonth7, totalCount: totalCountMonth7, uniqueCount: uniqueCountMonth7 },
            Month8: { startDate: startDateMonth8, endDate: endDateMonth8, totalCount: totalCountMonth8, uniqueCount: uniqueCountMonth8 },
            Month9: { startDate: startDateMonth9, endDate: endDateMonth9, totalCount: totalCountMonth9, uniqueCount: uniqueCountMonth9 },
            Month10: { startDate: startDateMonth10, endDate: endDateMonth10, totalCount: totalCountMonth10, uniqueCount: uniqueCountMonth10 },
            Month11: { startDate: startDateMonth11, endDate: endDateMonth11, totalCount: totalCountMonth11, uniqueCount: uniqueCountMonth11 },
            Month12: { startDate: startDateMonth12, endDate: endDateMonth12, totalCount: totalCountMonth12, uniqueCount: uniqueCountMonth12 },
        }

        res.status(200).json({
            statusCode: 200,
            message: "Retrieved short url visitor count successfully",
            data: resData
        });
    } catch (error) {
        log.error(error.message)
        console.log(error);
        return res.status(500).json({
            statusCode: 500,
            message: "Server error"
        });
    }

}