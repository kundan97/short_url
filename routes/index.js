const express = require('express');
const router = express.Router();
const shortUrlController = require('../controllers/shortUrlController')
const visitorInfoController = require('../controllers/visitorInfoController')


router.post("/api/url/getShortUrlList", shortUrlController.getShortUrlList);
router.post("/api/url/getVisitorInfoWeekWise", visitorInfoController.getVisitorInfoWeekWise);
router.post("/api/url/getVisitorInfoMonthWise", visitorInfoController.getVisitorInfoMonthWise);
router.post("/api/url/getVisitorInfoLastOneYear", visitorInfoController.getVisitorInfoLastOneYear);
// @route     GET /:code
// @desc      Redirect to long/original URL
router.get('/:code', shortUrlController.redirectLongUrl);


module.exports = router;
