const { createLogger, transports, format } = require('winston')
const { LOG_FILE_NAME } = process.env

module.exports.log = createLogger({
    transports: [
        new transports.File({
            filename: LOG_FILE_NAME,
            level: 'info',
            format: format.combine(format.timestamp(), format.json())
        })
    ]
})